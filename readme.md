## wsJournal

A module to facilitate websocket communication.
Used to store messages sent in order to use callbacks, error processing and timeouts.

### Usage

```
// init
this.journal = new journal(this);

// store when sending
this.journal.journalAdd(messageParse, cb, connection);
```