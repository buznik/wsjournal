// Websocket Journal
function trimForLog(message) {
	var trimmed = message;
	// remove images
	if (typeof trimmed.result != "undefined"
		&& trimmed.result != null
		&& typeof trimmed.result.image != "undefined") {
		trimmed.result.image = "...";
	}

	return trimmed;
}

module.exports = function wsJournal(wss, logEngine) {
	this.journal = {'_timer': {}, '_meta': {}};

	this.callback = function(message) {
		var messageParse = message;
		if (typeof message === "string") {
			messageParse = JSON.parse(message);
		}
		if (messageParse === null) return;

		var req,
			selectorField,
			connection = false;

		var is_broadcast = function() {
			return (messageParse != ""
				&& messageParse.id === "broadcast"
				&& typeof messageParse.result != "undefined");
		}

		if (is_broadcast()) {
			selectorField = messageParse.result.operationId;
		} else {
			selectorField = messageParse.id;
		}

		req = this.journalGet(selectorField);
		connection = this.journalGetConnection(selectorField);

		var cb = this.journal[selectorField + '_cb'];

		logEngine.debug('\n---   JOURNAL', typeof cb, '\n');

		if (req || is_broadcast()) {

			if (connection !== false
				&& (connection['readyState'] == 1 || connection == "server")) {

				if (typeof cb === "function") {
					cb.call(null, connection, messageParse);
				} else {
					if (connection != "server") {
						connection.send(JSON.stringify(message));
						logEngine.info('[Browser RX]', trimForLog(message));
					}

				}
			} else if (is_broadcast()) {
				// sorry
				wss.wss.clients.forEach(function each(client) {
					if (client['readyState'] == 1)
						client.send(JSON.stringify(message));
				});

			}
		} else {
			logEngine.error("[Backend] lost message: ", trimForLog(message));
		}


	};

	/** Journal clear */
	this.journalClear = function() {
		// cleared on connection restart
		this.journal = {'_timer': {}, '_meta': {}};
		return true;
	}

	/** Journal Add */
	this.journalAdd = function(message, cb, connection) {
		var _this = this;
		this.journal[message.id] = message;
		logEngine.debug("-- CB --", typeof cb, message.method);
		if (typeof cb === "function") {
			this.journal[message.id + '_cb'] = cb;
		}

		if (typeof connection != "undefined") {
			this.journal._meta[message.id] = {
				connection: connection
			};
		}

		return this.journal[message.id];
	}

	/** Journal item getter */
	this.journalGet = function(messageId) {
		return this.journal[messageId];
	}

	this.journalGetMeta = function(messageId) {
		return this.journal._meta[messageId];
	}

	this.journalGetConnection = function(messageId) {
		var meta = this.journal._meta[messageId];

		if (typeof meta != "undefined" && typeof meta.connection != "undefined") {
			return meta.connection;
		} else {
			return false;
		}

	}

	this.generateId = function() {
		return wss.serverClientName + "%%" + Date.now() + Math.ceil(Math.random()*1000);
	}


	this.processReply = function(message) {
		// get history from journal
		this.callback(message);
	}
}